import React from 'react';
import ReactDOM from 'react-dom';
import NettbureauForm from './components/Form';
const App = () => {
  return <NettbureauForm />;
};

ReactDOM.render(<App />, document.getElementById('root'));
