# Nettbureau AS front-end code, fall 2019

### What the project is about?

Front end code written as per the requirements of Nettbureau AS front-end code, fall 2019.

### Implemented Features:

- Norwegian phone number consist of only 8 digits.
- Name should consist minimum of 2 letters.
- A valid email address is required.
- Address should consist minimum of 4 letters.
- Form must submit data when valid inputs.

### Runnig the project

- `git clone https://bitbucket.org/y0natanF/nettbureau-front-end/src/master/`
- `npm install`
- `npm run start`

### What is missing in order to receive a positive response from the server?

## Analysis:

Error recieved:
`Access to XMLHttpRequest at 'https://heksemel.no/case/submit.php%E2%80%8B' from origin 'https://localhost:8081' has been blocked by CORS policy: Response to preflight request doesn't pass access control check: It does not have HTTP ok status.`

## Possible fix:

- Set Access-Control-Allow-Origin with the submit.php in the server.
