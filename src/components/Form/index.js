// React
import React, { useState } from 'react';
//Form Components
import { FormField, TextInput, Button, Icon } from 'evergreen-ui';
// Style, css
import '../../style/pageLayout.css';
import axios from 'axios';

const NettbureauForm = () => {
    const initialState = { name: '', nameValid: false, address: '', email: '', emailValid: false, addressValid: false, telephone: '', teleValid: false, dataSent: false, error: false };
    const [state, setState] = useState(initialState);

    const validateName = (name) => {
        const nameRe = /^[A-Za-z]+$/;
        if (nameRe.test(name) && name.length >= 2)
            setState((oldState) => ({ ...oldState, name, nameValid: true }));
        else if (nameRe.test(name) && name.length < 2)
            setState((oldState) => ({ ...oldState, name, nameValid: false }));
        else if (name.length === 0)
            setState((oldState) => ({ ...oldState, name: '', nameValid: false }));
    };

    const validateEmail = (email) => {
        const emailRe = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const emailValid = emailRe.test(String(email).toLowerCase());
        console.log()
        if (emailValid)
            setState((oldState) => ({ ...oldState, email, emailValid: true }));
        else if (!emailValid)
            setState((oldState) => ({ ...oldState, email, emailValid: false }));
    };

    const validateAddress = (address) => {
        if (address.length >= 4)
            setState((oldState) => ({ ...oldState, address, addressValid: true }));
        else
            setState((oldState) => ({ ...oldState, address, addressValid: false }));
    };

    const validateTelephone = (telephone) => {
        if (telephone.length < 8)
            setState((oldState) => ({ ...oldState, telephone, teleValid: false }));
        else if (telephone.length === 8)
            setState((oldState) => ({ ...oldState, telephone, teleValid: true }));
        else if (telephone.length > 8)
            setState((oldState) => ({ ...oldState, teleValid: true, }));
    };

    const handleSubmit = (e) => {
        const { nameValid, name, addressValid, address, emailValid, email, teleValid, telephone } = state;
        if (nameValid && addressValid && emailValid && teleValid) {
            console.log('> submit button triggered')
            e.preventDefault();
            const config = {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                }
            }
            axios.post('https://heksemel.no/case/submit.php​', {
                data: { name, email, address, telephone }
            }, config)
                .then(result => {
                    console.log('>> data sent >>', result.data);
                    setState((oldState) => ({
                        ...oldState,
                        dataSent: result.data.sent
                    }))
                })
                .catch(error => {
                    console.log('>> data send error >>', error);
                    setState((oldState) => ({ ...oldState, error: error.message }))
                });
        }
    }

    return (
        <div className='pageLayout'>
            <div className='contentLayout' >
                <FormField label='Name' >
                    <div style={{ display: 'flex' }}>
                        <TextInput
                            name="name"
                            placeholder="Name"
                            value={state.name}
                            onChange={(e) => { validateName(e.target.value) }}
                        />
                        {state.nameValid && <Icon icon="tick-circle" color="success" size={30} />}
                    </div>
                </FormField>
                <FormField label='E-Mail' >
                    <div style={{ display: 'flex' }}>
                        <TextInput
                            name="email"
                            placeholder="E-Mail"
                            value={state.email}
                            onChange={(e) => { validateEmail(e.target.value) }}
                        />
                        {state.emailValid && <Icon icon="tick-circle" color="success" size={30} />}
                    </div>
                </FormField>
                <FormField label='Address' >
                    <div style={{ display: 'flex' }}>
                        <TextInput
                            name="address"
                            placeholder="Address"
                            value={state.address}
                            onChange={(e) => { validateAddress(e.target.value) }}
                        />
                        {state.addressValid && <Icon icon="tick-circle" color="success" size={30} />}
                    </div>
                </FormField>
                <FormField label='Telephone' >
                    <div style={{ display: 'flex' }}>
                        <TextInput name="telephone"
                            placeholder="Telephone"
                            type="number"
                            max={8}
                            value={state.telephone}
                            onChange={(e) => { validateTelephone(e.target.value) }}
                        />
                        {state.teleValid && <Icon icon="tick-circle" color="success" size={30} />}
                    </div>
                </FormField>
                <Button appearance="primary" style={{ margin: '5% 30%' }} onClick={(e) => { handleSubmit(e) }}>Submit</Button>
            </div>
        </div>
    )
};
export default NettbureauForm;